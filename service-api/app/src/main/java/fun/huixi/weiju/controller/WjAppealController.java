package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.AppealBusiness;
import fun.huixi.weiju.page.PageData;
import fun.huixi.weiju.pojo.vo.appeal.AppealItemVO;
import fun.huixi.weiju.pojo.dto.appeal.PageQueryAppealDTO;
import fun.huixi.weiju.util.CommonUtil;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 诉求表 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjAppeal")
public class WjAppealController extends BaseController {

    @Resource
    private AppealBusiness appealBusiness;


    /**
     *  分页查询诉求
     * @Author 叶秋
     * @Date 2021/11/15 22:07
     * @param pageQueryAppealDTO 分页查询参数
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("pageQueryAppeal")
    public ResultData<PageData<AppealItemVO>> pageQueryAppeal(@RequestBody @Valid PageQueryAppealDTO pageQueryAppealDTO){

        Integer userId = CommonUtil.getNowUserId();

        PageData<AppealItemVO> pageData = appealBusiness.pageQueryAppeal(userId, pageQueryAppealDTO);

        return ResultData.ok(pageData);
    }




}

