package fun.huixi.weiju.controller;


import fun.huixi.weiju.pojo.entity.Area;
import fun.huixi.weiju.service.AreaService;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;

/**
 *  前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/area")
public class AreaController extends BaseController {



}

