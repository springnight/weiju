package fun.huixi.weiju.controller;


import fun.huixi.weiju.pojo.entity.WjUserTag;
import fun.huixi.weiju.service.WjUserTagService;
import fun.huixi.weiju.util.wrapper.ResultData;
import lombok.Getter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户所存储的标签 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjUserTag")
public class WjUserTagController extends BaseController {


    @Resource
    private WjUserTagService wjUserTagService;

    /**
     *  查询所有的用户标签
     * @Author 叶秋
     * @Date 2021/11/8 23:54
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @GetMapping("queryAllUserTag")
    public ResultData queryAllUserTag(){

        List<WjUserTag> list = wjUserTagService.list();

        return ResultData.ok();
    }

}

