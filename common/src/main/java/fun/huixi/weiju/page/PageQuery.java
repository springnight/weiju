package fun.huixi.weiju.page;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 分页查询的请求参数封装
 *
 * @author 碧海青天夜夜心
 * @sice 2019年10月15日 16:05:17
 */
@Data
public class PageQuery {

    /**
     * 每页的条数
     */
    @NotNull(message = "每页的条数不能为空")
    private Integer size;

    /**
     * 页编码(第几页)
     */
    @NotNull(message = "页码不能为空")
    private Integer current;


}
