package fun.huixi.weiju.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import fun.huixi.weiju.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户所存储的标签
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("wj_user_tag")
public class WjUserTag extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 用户标签表主键
     */
    @TableId(value = "user_tag_id", type = IdType.AUTO)
    private Integer userTagId;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 标签id
     */
    @TableField("tag_id")
    private Integer tagId;

    /**
     * 标签名字
     */
    @TableField("tag_value")
    private String tagValue;



}
