package fun.huixi.weiju.pojo.dto.dynamic;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *  保存动态评论
 * @Author 叶秋
 * @Date 2021/11/29 14:43
 * @param
 * @return
 **/
@Data
public class SaveDynamicCommentDTO {


    @NotNull(message = "动态id不能为空")
    private Integer dynamicId;

    /**
     * 评论的内容
     */
    @NotBlank(message = "评论的内容不能为空")
    private String content;



}
