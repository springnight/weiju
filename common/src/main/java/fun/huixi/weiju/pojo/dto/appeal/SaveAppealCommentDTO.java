package fun.huixi.weiju.pojo.dto.appeal;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *  保存帖子评论所需要的参数
 * @Author 叶秋 
 * @Date 2021/11/16 11:34
 * @param 
 * @return 
 **/
@Data
public class SaveAppealCommentDTO {

    /**
     * 诉求id
     */
    @NotNull(message = "诉求id不能为空")
    private Integer appealId;

    /**
     * 评论的内容
     */
    @NotBlank(message = "评论内容不能为空")
    private String content;

    /**
     * 评论所加的贴图（只能是一张）
     */
    private String url;

}
