package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.Area;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface AreaMapper extends BaseMapper<Area> {

}
