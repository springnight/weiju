package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjAppealTag;
import fun.huixi.weiju.mapper.WjAppealTagMapper;
import fun.huixi.weiju.service.WjAppealTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 诉求-对应标签 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjAppealTagServiceImpl extends ServiceImpl<WjAppealTagMapper, WjAppealTag> implements WjAppealTagService {



    /**
     * 根据诉求标签id查询对应的诉求标签
     *
     * @param appealTagIds 诉求id集合
     * @return 标签id <-> 标签详情
     * @Author 叶秋
     * @Date 2021/11/10 22:45
     **/
    @Override
    public Map<Integer, WjAppealTag> queryAppealTagByTagIds(Collection appealTagIds) {

        List<WjAppealTag> list = this.lambdaQuery()
                .in(WjAppealTag::getTagId, appealTagIds)
                .list();

        Map<Integer, WjAppealTag> collect = list.stream().collect(Collectors.toMap(WjAppealTag::getTagId, item -> item));

        return collect;
    }

}
