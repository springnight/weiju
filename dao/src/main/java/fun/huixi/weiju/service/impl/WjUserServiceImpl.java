package fun.huixi.weiju.service.impl;

import cn.hutool.core.bean.BeanUtil;
import fun.huixi.weiju.pojo.entity.WjUser;
import fun.huixi.weiju.mapper.WjUserMapper;
import fun.huixi.weiju.pojo.vo.user.UserTokenVO;
import fun.huixi.weiju.service.WjUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjUserServiceImpl extends ServiceImpl<WjUserMapper, WjUser> implements WjUserService {


    /**
     * 查询用户信息 用于token加密处理
     *
     * @param userId 普通用户id
     * @return com.xinzhengcheng.mingji.pojo.vo.user.UserTokenVO
     * @Author 叶秋
     * @Date 2021/9/14 10:47
     **/
    @Override
    public UserTokenVO queryUserTokenInfo(Integer userId) {
        UserTokenVO userTokenVO = new UserTokenVO();

        WjUser byId = this.getById(userId);

        BeanUtil.copyProperties(byId, userTokenVO);

        return userTokenVO;
    }

    /**
     * 查询用户名是否存在
     *
     * @param nickName 用户昵称
     * @return fun.huixi.weiju.pojo.entity.WjUser
     * @Author 叶秋
     * @Date 2021/11/2 23:33
     **/
    @Override
    public WjUser loadUserByNickName(String nickName) {

        WjUser one = this.lambdaQuery()
                .eq(WjUser::getNickName, nickName)
                .one();

        return one;
    }

    /**
     * 判断用户名称是否存在
     *
     * @param nickName
     * @return boolean
     * @Author 叶秋
     * @Date 2021/11/10 23:36
     **/
    @Override
    public boolean judgeNickNameExist(String nickName) {

        Integer count = this.lambdaQuery()
                .eq(WjUser::getNickName, nickName)
                .count();

        if(count > 0){
            return true;
        }

        return false;
    }

    /**
     * 判断该邮箱是否存在
     *
     * @param email
     * @return boolean
     * @Author 叶秋
     * @Date 2021/11/10 23:38
     **/
    @Override
    public boolean judgeEmailExist(String email) {

        Integer count = this.lambdaQuery()
                .eq(WjUser::getEmail, email)
                .count();

        if(count > 0){
            return true;
        }

        return false;
    }

    /**
     * 根据用户id集合查询用户的基本信息
     *
     * @param userIds 用户id集合
     * @return java.util.Map<java.lang.Integer, fun.huixi.weiju.pojo.entity.WjUser>
     * @Author 叶秋
     * @Date 2021/11/15 22:22
     **/
    @Override
    public Map<Integer, WjUser> queryUserInfoByUserIds(Set<Integer> userIds) {

        List<WjUser> list = this.lambdaQuery()
                .select(WjUser::getUserId, WjUser::getNickName, WjUser::getHeadPortrait)
                .in(WjUser::getUserId, userIds)
                .list();

        Map<Integer, WjUser> collect = list.stream().collect(Collectors.toMap(WjUser::getUserId, item -> item));

        return collect;
    }
}
