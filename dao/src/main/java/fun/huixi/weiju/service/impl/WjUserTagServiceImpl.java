package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjUserTag;
import fun.huixi.weiju.mapper.WjUserTagMapper;
import fun.huixi.weiju.service.WjUserTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户所存储的标签 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjUserTagServiceImpl extends ServiceImpl<WjUserTagMapper, WjUserTag> implements WjUserTagService {

}
