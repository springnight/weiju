package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjAppealEndorse;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 此表为记录诉求点赞用途，用来判断此人是否已经点赞 服务类
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealEndorseService extends IService<WjAppealEndorse> {



    /**
     *  查询用户是否点赞了该动态
     * @Author 叶秋
     * @Date 2021/11/15 22:30
     * @param userId 用户id
     * @param appealIdList 诉求id
     * @return 诉求id <-> 是否点赞
     **/
    Map<Integer, Boolean> queryUserOrEndorseByUserId(Integer userId, List<Integer> appealIdList);



    /**
     *  判断用户是否点赞了该诉求
     * @Author 叶秋
     * @Date 2021/11/16 10:48
     * @param userId 用户id
     * @param appealId 诉求id
     * @return java.lang.Boolean
     **/
    Boolean judgeUserOrEndorse(Integer userId, Integer appealId);


    /**
     *  点赞该诉求
     * @Author 叶秋
     * @Date 2021/11/16 10:51
     * @param userId 用户id
     * @param appealId 诉求id
     * @return void
     **/
    void endorseAppeal(Integer userId, Integer appealId);

    /**
     *  取消点赞该诉求
     * @Author 叶秋
     * @Date 2021/11/16 10:51
     * @param userId 用户id
     * @param appealId 诉求id
     * @return void
     **/
    void cancelEndorseAppeal(Integer userId, Integer appealId);

}
