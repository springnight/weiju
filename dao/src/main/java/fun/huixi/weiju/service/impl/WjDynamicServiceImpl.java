package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjDynamic;
import fun.huixi.weiju.mapper.WjDynamicMapper;
import fun.huixi.weiju.service.WjDynamicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 动态表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjDynamicServiceImpl extends ServiceImpl<WjDynamicMapper, WjDynamic> implements WjDynamicService {

}
