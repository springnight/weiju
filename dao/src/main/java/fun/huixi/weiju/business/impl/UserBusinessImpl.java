package fun.huixi.weiju.business.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import fun.huixi.weiju.business.UserBusiness;
import fun.huixi.weiju.exception.BusinessException;
import fun.huixi.weiju.pojo.entity.WjUser;
import fun.huixi.weiju.pojo.vo.user.UserRegisterVO;
import fun.huixi.weiju.service.WjUserService;
import fun.huixi.weiju.service.WjUserTagService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *  用户业务实现类
 * @Author 叶秋 
 * @Date 2021/11/8 23:50
 * @param 
 * @return 
 **/
@Service
public class UserBusinessImpl implements UserBusiness {

    @Resource
    private WjUserTagService wjUserTagService;

    @Resource
    private WjUserService userService;


    /**
     * 用户注册方法
     *
     * @param userRegisterVO
     * @return void
     * @Author 叶秋
     * @Date 2021/11/10 23:30
     **/
    @Override
    public void userRegister(UserRegisterVO userRegisterVO) {

        String nickName = userRegisterVO.getNickName();
        String email = userRegisterVO.getEmail();
        String password = userRegisterVO.getPassword();
        String sex = userRegisterVO.getSex();

        boolean nickNameExist = userService.judgeNickNameExist(nickName);
        if(nickNameExist){
            throw new BusinessException("该昵称已存在");
        }

        if(StrUtil.isNotEmpty(email)){
            boolean emailExist = userService.judgeEmailExist(email);
            if(emailExist){
                throw new BusinessException("该邮箱已经存在");
            }
        }

        WjUser wjUser = new WjUser();
        wjUser.setNickName(nickName);
        wjUser.setEmail(email);
        wjUser.setPassword(SecureUtil.md5(password));
        wjUser.setSex(sex);

        boolean save = userService.save(wjUser);

        if(!save){
            throw new BusinessException("注册失败");
        }


    }



}
