create table area
(
    code        int                     not null comment '地区码'
        primary key,
    name        varchar(100) default '' null comment '地区名称',
    parent_code int          default 0  null comment '父级地区码',
    area_type   int          default 0  null comment '区域类型(0无 1国家 2省 3市 4区 5街道)'
);

create table wj_appeal
(
    appeal_id     int auto_increment comment '诉求的id'
        primary key,
    user_id       int           not null comment '诉求对应的用户id',
    title         varchar(100)  not null comment '诉求的标题',
    content       text          null comment '诉求的内容',
    endorse_count int default 0 null comment '诉求点赞量',
    comment_count int default 0 null comment '诉求的评论量',
    browse_count  int default 0 null comment '诉求浏览量',
    update_count  int default 0 null comment '修改次数，需要限定修改次数',
    location_auth tinyint(1)    null comment '该动态是否标识了位置信息  0未标识 1已标识',
    create_time   datetime      null comment '创建时间',
    update_time   datetime      null comment '修改时间',
    del_flag      tinyint(1)    null comment '逻辑删除标识',
    status_flag   tinyint(1)    null comment '状态码 ',
    version       int           null comment '乐观锁'
)
    comment '诉求表';

create table wj_appeal_address
(
    appeal_address_id    int            not null comment '诉求具体位置的主键',
    appeal_id            int            not null comment '对应的诉求id',
    latitude             decimal(10, 7) null comment '纬度',
    longitude            decimal(10, 7) null comment '经度',
    nation               int            null comment '国家',
    province             int            null comment '省',
    city                 int            null comment '市',
    district             int            null comment '区',
    adcode               int            null comment '行政区划代码
行政区划代码',
    detailed_address     varchar(32)    null comment '详细地址',
    standby_address      varchar(32)    null comment '备用地址(冗余)',
    standby_address_code int            null comment '备用的地址code(冗余)',
    create_time          datetime       null comment '创建时间',
    update_time          datetime       null comment '修改时间',
    del_flag             tinyint(1)     null comment '逻辑删除标识',
    status_flag          tinyint(1)     null comment '状态码 ',
    version              int            null comment '乐观锁',
    constraint wj_appeal_address_appeal_address_id_uindex
        unique (appeal_address_id)
)
    comment '存储诉求的地址信息';

alter table wj_appeal_address
    add primary key (appeal_address_id);

create table wj_appeal_comment
(
    appeal_comment_id int auto_increment comment '诉求评论的id'
        primary key,
    appeal_id         int                     not null comment '诉求id',
    user_id           int                     not null comment '评论人的id',
    content           varchar(255)            null comment '评论的内容',
    url               varchar(100) default '' null comment '评论所加的贴图（只能是一张）',
    endorse_count     int          default 0  null comment '评论的点赞数||赞同数',
    comment_count     int          default 0  null comment '对评论评论的次数',
    create_time       datetime                null comment '创建时间',
    update_time       datetime                null comment '修改时间',
    del_flag          tinyint(1)              null comment '逻辑删除标识',
    status_flag       tinyint(1)              null comment '状态码',
    version           int                     null comment '乐观锁'
)
    comment '诉求-评论';

create table wj_appeal_endorse
(
    appeal_endorse_id int auto_increment comment '此表的id'
        primary key,
    appeal_id         int        not null comment '对应的诉求id',
    user_id           int        not null comment '点赞人的id',
    create_time       datetime   null comment '创建时间',
    update_time       datetime   null comment '修改时间',
    del_flag          tinyint(1) null comment '逻辑删除标识',
    status_flag       tinyint(1) null comment '状态码 ',
    version           int        null comment '乐观锁'
)
    comment '此表为记录诉求点赞用途，用来判断此人是否已经点赞';

create table wj_appeal_material
(
    appeal_material_id int auto_increment comment '诉求素材表的id'
        primary key,
    appeal_id          int          not null comment '诉求id',
    url                varchar(200) not null comment '素材的url地址（存放在OSS）',
    create_time        datetime     null comment '创建时间',
    update_time        datetime     null comment '修改时间',
    del_flag           tinyint(1)   null comment '逻辑删除标识',
    status_flag        tinyint(1)   null comment '状态码',
    version            int          null comment '乐观锁'
)
    comment '诉求素材表-存储素材涉及的图片，或者大文件';

create table wj_appeal_tag
(
    tag_id      int auto_increment comment '主键'
        primary key,
    appeal_id   int          not null comment '对应的诉求id',
    tag_name    varchar(10)  null comment '标签的名字',
    explains    varchar(100) null comment '标签的解释（冗余）',
    url         varchar(100) null comment '标签可能需要的地址值(冗余)',
    create_time datetime     null comment '创建时间',
    update_time datetime     null comment '修改时间',
    del_flag    tinyint(1)   null comment '逻辑删除标识',
    status_flag tinyint(1)   null comment '状态码',
    version     int          null comment '乐观锁'
)
    comment '诉求-对应标签';

create table wj_chat
(
    chat_id       int auto_increment comment 'id'
        primary key,
    appeal_id     int          not null comment '诉求id',
    type          tinyint(1)   null comment '聊天室类型 0私聊 1群聊',
    title         varchar(50)  null comment '标题',
    people_number int          null comment '限制人数',
    url           varchar(100) null comment '聊天室的图片（一对一聊天就用双方的头像组合成一张，一对多就多张组合起来）',
    create_time   datetime     null comment '创建时间',
    update_time   datetime     null comment '修改时间',
    del_flag      tinyint(1)   null comment '逻辑删除标识',
    status_flag   tinyint(1)   null comment '状态码',
    version       int          null comment '乐观锁'
)
    comment '聊天室（聊天列表）';

create table wj_chat_record
(
    chat_record_id int auto_increment comment '主键'
        primary key,
    chat_id        int           not null comment '聊天室id',
    user_id        int           not null comment '用户id',
    chat_order     int default 1 not null comment '聊天的次序（谁说在前，谁说在后）',
    content        varchar(255)  not null comment '聊天的内容',
    type           int default 1 not null comment '内容类型（1：文字 2：图片 3：视频）',
    create_time    datetime      not null comment '创建时间',
    update_time    datetime      null comment '修改时间',
    del_flag       tinyint(1)    null comment '逻辑删除标识',
    status_flag    tinyint(1)    null comment '状态码 ',
    version        int           null comment '乐观锁'
)
    comment '聊天室-聊天记录';

create table wj_chat_user
(
    chat_user_id int auto_increment comment '主键'
        primary key,
    chat_id      int        not null comment '聊天室id',
    user_id      int        not null comment '聊天室对应的用户id（一对一，一对多）',
    create_time  datetime   not null comment '创建时间（或者加入时间）',
    quit_time    datetime   null comment '退出聊天的时间（一对多的聊天室可以退出群聊，需要个退出时间）',
    update_time  datetime   null comment '修改时间',
    del_flag     tinyint(1) null comment '逻辑删除标识',
    status_flag  tinyint(1) null comment '状态码',
    version      int        null comment '乐观锁'
)
    comment '聊天室对应的用户';

create table wj_dynamic
(
    dynamic_id    int           not null comment '动态id'
        primary key,
    user_id       int           not null comment '关联的用户id',
    content       varchar(255)  not null comment '动态的内容（没有标题之类的）',
    tag_dict_id   int           null comment '动态标签，对应的字典id',
    url           varchar(100)  null comment '动态所发素材的url地址，多个用逗号隔开',
    endorse_count int default 0 null comment '动态 点赞数||赞同数',
    comment_count int default 0 null comment '动态 评论数',
    browse_count  int default 0 null comment '动态 浏览量',
    create_time   datetime      null comment '创建时间',
    update_time   datetime      null comment '修改时间',
    del_flag      tinyint(1)    null comment '逻辑删除标识',
    status_flag   tinyint(1)    null comment '状态码',
    version       int           null comment '乐观锁'
)
    comment '动态表';

create table wj_dynamic_comment
(
    dynamic_comment_id int           not null comment '动态评论id'
        primary key,
    dynamic_id         int           not null comment '对应的动态id',
    user_id            int           not null comment '发起评论的用户id',
    content            varchar(100)  not null comment '评论的内容',
    endorse_count      int default 0 null comment '评论的点赞数',
    comment_count      int default 0 null comment '评价评论的次数',
    create_time        datetime      null comment '创建时间',
    update_time        datetime      null comment '修改时间',
    del_flag           tinyint(1)    null comment '逻辑删除标识',
    status_flag        tinyint(1)    null comment '状态码',
    version            int           null comment '乐观锁'
)
    comment '动态评论表';

create table wj_dynamic_endorse
(
    dynamic_endorse_id int auto_increment comment '动态点赞表的id'
        primary key,
    dynamic_id         int        not null comment '动态id',
    user_id            int        not null comment '点赞人的id',
    create_time        datetime   null comment '创建时间',
    update_time        datetime   null comment '修改时间',
    del_flag           tinyint(1) null comment '逻辑删除标识',
    status_flag        tinyint(1) null comment '状态码',
    version            int        null comment '乐观锁'
)
    comment '动态的点赞表';

create table wj_user
(
    user_id       int auto_increment comment '用户id'
        primary key,
    nick_name     varchar(50)  not null comment '昵称',
    head_portrait varchar(255) null comment '头像对应的URL地址',
    sex           varchar(50)  null comment '性别 改用String，活泼一点。自定义都可以',
    mobile        varchar(50)  null comment '手机号',
    email         varchar(50)  null comment '电子邮箱',
    signature     varchar(50)  null comment '个性签名（冗余）',
    introduce     text         null comment '简介',
    address       varchar(50)  null comment '地址',
    password      varchar(50)  null comment '密码（冗余）',
    create_time   datetime     null comment '创建时间',
    update_time   datetime     null comment '修改时间',
    del_flag      tinyint(1)   null comment '逻辑删除标识',
    status_flag   tinyint(1)   null comment '状态码',
    version       int          null comment '乐观锁'
)
    comment '用户信息表';

create table wj_user_tag
(
    user_tag_id int auto_increment comment '用户标签表主键'
        primary key,
    user_id     int         null comment '用户id',
    tag_id      int         null comment '标签id',
    tag_value   varchar(10) null comment '标签名字',
    create_time datetime    null comment '创建时间',
    update_time datetime    null comment '修改时间',
    del_flag    tinyint(1)  null comment '逻辑删除标识',
    status_flag tinyint(1)  null comment '状态码',
    version     int         null comment '乐观锁'
)
    comment '用户所存储的标签';

create table wj_user_tag_center
(
    u_t_id      int auto_increment comment '主键'
        primary key,
    user_id     int        null comment '用户id',
    tag_id      int        null comment '用户对应的标签id',
    create_time datetime   null,
    update_time datetime   null,
    del_flag    tinyint(1) null,
    status_flag tinyint(1) null,
    version     int        null comment '乐观锁'
)
    comment '用户与标签的中间表';


